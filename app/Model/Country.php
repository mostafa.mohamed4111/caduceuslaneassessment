<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table    =   'countries';
    protected $guarded  =   ['id'];

    public function users() {
        return $this->hasMany(User::class , 'country_id', 'id');
    }
}
