<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Specialiest extends Model
{
    protected $table    =   'specialties';
    protected $guarded  =   ['id'];
}
