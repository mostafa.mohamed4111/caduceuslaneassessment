<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table    =   'notifications';
    protected $guarded  =   ['id'];

    public function patentCase() {
        return $this->belongsTo(PatientCase::class, 'patent_case_id', 'id');
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
