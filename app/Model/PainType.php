<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class PainType extends Model
{
    protected $table    =   'pain_types';
    protected $guarded  =   ['id'];

    public function users() {
        return $this->hasMany(User::class, 'pain_type_id' , 'id');
    }
}
