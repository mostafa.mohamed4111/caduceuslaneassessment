<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class PatientCase extends Model
{
    protected $table    =   'patients_cases';
    protected $guarded  =   ['id'];

    const NeedDate      =   0;
    const hasDate       =   1;
    const doctorCancel  =   2;
    const userCancel    =   3;

    public function patient() {
        return $this->belongsTo(User::class , 'patient_id', 'id');
    }

    public function doctor() {
        return $this->belongsTo(User::class , 'doctor_id', 'id');
    }

    public function pain() {
        return $this->belongsTo(PainType::class, 'pain_type_id' , 'id');
    }
}
