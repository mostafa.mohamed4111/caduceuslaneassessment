<?php

namespace App;

use App\Model\Country;
use App\Model\PainType;
use App\Model\PatientCase;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username', 'user_type', 'first_name',
        'last_name', 'mobile', 'birthday', 'gender', 'email', 'country_id',
        'pain_type_id', 'occupation', 'is_active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'birthday' => 'datetime',
    ];

    protected $appends  =   ['type', 'name'];

    const Admin   = 1;
    const Doctor  = 2;
    const Patient  = 3;

    const Male    = 1;
    const Female  = 2;

    public function patientCases() {
        return $this->hasMany(PatientCase::class, 'patient_id', 'id');
    }

    public function doctorCases() {
        return $this->hasMany(PatientCase::class, 'doctor_id', 'id');
    }

    public function country() {
        return $this->belongsTo(Country::class, 'country_id', 'id');
    }

    public function painType() {
        return $this->belongsTo(PainType::class, 'pain_type_id', 'id');
    }

    public function getTypeAttribute() {
        $types = [
            self::Admin =>  'dashboard.components.admin',
            self::Doctor =>  'dashboard.components.doctor',
            self::Patient =>  'dashboard.components.patient',
        ];

        return $types[$this->user_type];
    }

    public function needCompeteInfo() {
        return is_null($this->first_name) && is_null($this->last_name) && is_null($this->mobile) && is_null($this->birthday) && is_null($this->gander);
    }

    public function getNameAttribute() {
        return "$this->first_name $this->last_name";
    }
}
