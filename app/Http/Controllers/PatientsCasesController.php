<?php

namespace App\Http\Controllers;

use App\Model\PatientCase;
use App\Notifications\NewAppointment;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Illuminate\Validation\Rule;

class PatientsCasesController extends Controller
{
    public function store(Request $request) {
        $this->validate($request, [
            'pain_type_id'  =>  'required|exists:pain_types,id',
            'case_description' =>   'required',
            'user_type'     => Rule::requiredIf(function (){return auth()->user()->user_type !== User::Patient;})
        ]);
        PatientCase::query()->create([
            'reference'         =>  Carbon::now()->timestamp,
            'patient_id'        =>  auth()->id(),
            'pain_type_id'      =>  $request->input('pain_type_id'),
            'case_description'  =>  $request->input('case_description')
        ]);

        return redirect()->route('home')->with('success', 'case created successfully');
    }

    public function patientConfirmation(Request $request) {
        auth()->user()->patientCases()->where('id', $request->input('id'))->update([
            'patient_confirmation'  =>  $request->input('patient_confirmation'),
        ]
            + ($request->input('patient_confirmation') == 2 ? ['appointment'=>null,'doctor_confirmation'=> 0] : [])
        );

        return redirect()->back()->with('success', 'your action has been saved');
    }

    public function doctorConfirmation(Request $request) {
        auth()->user()->doctorCases()->where('id', $request->input('id'))->update([
                'doctor_confirmation'  =>  $request->input('doctor_confirmation'),
            ]
            + ($request->input('doctor_confirmation') == 2 ? ['appointment'=>null,'patient_confirmation'=> 0] : [])
        );

        return redirect()->back()->with('success', 'your action has been saved');
    }

    public function setAppointment($id) {
        if(auth()->user()->user_type !== User::Admin) {
            abort(401,'You Cannot Login This Page');
        }
        $case = PatientCase::query()->find($id);
        return view('dashboard.components.set-appointment', ['case'=>$case]);
    }

    public function setNewAppointment(Request $request) {
        $this->validate($request, [
            'case_id' =>  'required|exists:patients_cases,id',
            'doctor_id' =>  'required|exists:users,id',
            'appointment'   =>  'required|date',
        ]);
        $case = PatientCase::query()->where('id', $request->input('case_id'))->firstOrFail();
        $case->update([
            'doctor_id' =>  $request->input('doctor_id'),
            'appointment' =>  $request->input('appointment'),
            'patient_confirmation'  =>  0,
            'doctor_confirmation'  =>  0,
        ]);
        Notification::send([$case->patient, $case->doctor], new NewAppointment($case));
        return redirect()->route('home')->with('success', 'appointment has been set');
    }
}
