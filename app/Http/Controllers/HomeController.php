<?php

namespace App\Http\Controllers;

use App\Model\PatientCase;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = $this->getData();
        return view('dashboard.home.index', $data);
    }

    private function getData() {
        switch (auth()->user()->user_type) {
            case User::Patient:
                return $this->patientData();
            case User::Doctor:
                return $this->doctorData();
            case User::Admin:
                return $this->adminData();
            default:
                return [];
        }
    }

    private function patientData() {
        $cases = auth()->user()
            ->patientCases()
            ->get();
        return ['cases' => $cases];
    }

    private function doctorData() {
        $cases = auth()->user()
            ->doctorCases()
            ->get();
        return ['cases' => $cases];
    }

    private function adminData() {
        $cases = PatientCase::query()
            ->orderByDesc('patient_confirmation')
            ->orderByDesc('doctor_confirmation')
            ->orderBy('appointment')
            ->get();
        return ['cases' => $cases];
    }
}
