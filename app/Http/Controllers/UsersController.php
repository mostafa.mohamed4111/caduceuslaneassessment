<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function update(Request $request, $id) {
        $this->validate($request, [
            'first_name'    =>  'required',
            'last_name'     =>  'required',
            'email'         =>  'required|email',
            'mobile'        =>  'required',
            'gender'        =>  "required|in:1,2",
            'country_id'    =>  'required|exists:countries,id',
            'occupation'    =>  'required',
            'birthday'      =>  'required',
        ]);

        auth()->user()->update([
            'first_name'    =>  $request->input('first_name'),
            'last_name'     =>  $request->input('last_name'),
            'email'         =>  $request->input('email'),
            'mobile'        =>  $request->input('mobile'),
            'gender'        =>  $request->input('gender'),
            'country_id'    =>  $request->input('country_id'),
            'occupation'    =>  $request->input('occupation'),
            'birthday'      =>  $request->input('birthday'),
        ]);

        return redirect()->route('home')->with('success', 'Update Successfully');
    }

    public function getDoctors(Request $request) {
        $doctors = User::query()->where('doctor_specialist_id', $request->input('id'))->get();
        return response()->json(['doctors'=>$doctors]);
    }
}
