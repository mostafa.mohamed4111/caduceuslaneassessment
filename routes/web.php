<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth:web'])->group(function (){
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('user' , 'UsersController');
    Route::resource('patients-cases' , 'PatientsCasesController');
    Route::get('patientConfirmation' , 'PatientsCasesController@patientConfirmation')->name('patients-cases.patientConfirmation');
    Route::get('doctorConfirmation' , 'PatientsCasesController@doctorConfirmation')->name('patients-cases.doctorConfirmation');
    Route::get('setAppointment/{id}' , 'PatientsCasesController@setAppointment')->name('patients-cases.setAppointment');
    Route::post('setAppointment' , 'PatientsCasesController@setNewAppointment')->name('setAppointment');
    Route::post('getDoctors' , 'UsersController@getDoctors')->name('getDoctors');
});

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Route::get('logout', function (){
    auth()->logout();
    return redirect()->route('welcome');
});
Auth::routes();


