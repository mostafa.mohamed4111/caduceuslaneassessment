@extends('layouts.main')
@section('content')
    <div class="mdl-card mdl-shadow--2dp employer-form">
        <div class="mdl-card__title">
            <h2>Set Appointment</h2>
        </div>

        <div class="mdl-card__supporting-text">
            <form action="{{ route('setAppointment') }}" method="post" class="form">
                @csrf
                <input type="hidden" name="case_id" value="{{ $case->id }}">
                <div class="form__article">
                    <div class="mdl-grid">
                        <div class="mdl-cell mdl-cell--6-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select full-size">
                                <select class="mdl-textfield__input specialist" required style="color: #00bcd4;background: #444;">
                                    <option value="">Select Specialist</option>
                                    @foreach(\App\Model\Specialiest::query()->get() as $sp)
                                        <option value="{{ $sp->id }}">{{ $sp->name }}</option>
                                    @endforeach
                                </select>

                            </div>
                        </div>

                        <div class="mdl-cell mdl-cell--6-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select full-size">
                                <select class="mdl-textfield__input doctors" name="doctor_id" required style="color: #00bcd4;background: #444;">
                                    <option value="">Select Doctor</option>

                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="mdl-grid">
                        <div class="mdl-cell mdl-cell--6-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <input class="mdl-textfield__input" type="text" id="datetime" value="" name="appointment" required autocomplete="off"/>
                            <label class="mdl-textfield__label" for="birthday">Appointment Date</label>
                        </div>
                    </div>
                </div>

                <div class="form__action">
                    <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
                        Submit
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(function(){
            $('#datetime').datetimepicker({
                theme: 'dark'
            });
            $('.specialist').on('change', function () {
                let id = $(this).find('option:selected').val();
                $.ajax({
                    method: 'post',
                    url: "{{ route('getDoctors') }}",
                    data: {_token:"{{csrf_token()}}", id:id},
                    success: (response) => {
                        console.log(response)
                        $('.doctors').find('option:not(:first)').remove();
                        $.each(response.doctors, function(index, doctor){
                            let option = "<option value='"+doctor.id+"'>"+doctor.name+"</option>";
                            $('.doctors').append(option);
                        });
                    },
                    error: (response) => {}
                });
            })
        })
    </script>
@endpush
