<div class="mdl-grid ui-tables">
    <div class="mdl-cell mdl-cell--12-col-desktop mdl-cell--12-col-tablet mdl-cell--12-col-phone">
        <div class="mdl-card mdl-shadow--2dp">
            <div class="mdl-card__title">
                <h1 class="mdl-card__title-text">All Cases</h1>
            </div>
            <div class="mdl-card__supporting-text no-padding">
                <table class="mdl-data-table mdl-js-data-table bordered-table" data-upgraded=",MaterialDataTable">
                    <thead>
                    <tr>
                        <th class="mdl-data-table__cell--non-numeric">Reference #no</th>
                        <th class="mdl-data-table__cell--non-numeric">Pain</th>
                        <th class="mdl-data-table__cell--non-numeric">Description</th>
                        <th class="mdl-data-table__cell--non-numeric">Patient</th>
                        <th class="mdl-data-table__cell--non-numeric">Doctor</th>
                        <th class="mdl-data-table__cell--non-numeric">Appointment</th>
                        <th class="mdl-data-table__cell--non-numeric">Status</th>
                        <th class="mdl-data-table__cell--non-numeric">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($cases as $case)
                        <tr>
                            <td class="mdl-data-table__cell--non-numeric">{{ $case->reference }}</td>
                            <td class="mdl-data-table__cell--non-numeric">{{ $case->pain->name }}</td>
                            <td class="mdl-data-table__cell--non-numeric"><span class="limit-str">{{ $case->case_description }}</span></td>
                            <td class="mdl-data-table__cell--non-numeric">{{ optional($case->patient)->name }}</td>
                            <td class="mdl-data-table__cell--non-numeric">{{ optional($case->doctor)->name }}</td>
                            <td class="mdl-data-table__cell--non-numeric">{{ $case->appointment }}</td>
                            <td class="mdl-data-table__cell--non-numeric">
                                <ul>
                                    @if($case->patient_confirmation == 1)
                                        <li>Patient Confirm Appointment</li>
                                    @elseif($case->patient_confirmation == 2)
                                        <li>Patient Need Reschedule Appointment</li>
                                    @endif
                                    @if($case->doctor_confirmation == 1)
                                        <li>Doctor Confirm Appointment</li>
                                    @elseif($case->doctor_confirmation == 2)
                                        <li>Doctor Need Reschedule Appointment</li>
                                    @endif
                                </ul>
                            </td>
                            <td class="mdl-data-table__cell--non-numeric">
                                @if($case->appointment && ($case->patient_confirmation == 0 || $case->doctor_confirmation == 0))
                                        Witting Confirmation
                                @elseif($case->patient_confirmation == 1 && $case->doctor_confirmation == 1)
                                    Confirmed
                                @elseif($case->patient_confirmation == 2 || $case->doctor_confirmation == 2)
                                    <a href="{{ route('patients-cases.setAppointment', $case->id) }}" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect button--colored-red">Reschedule</a>
                                @else
                                    <a href="{{ route('patients-cases.setAppointment', $case->id) }}" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect button--colored-purple">Set Appointment</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
