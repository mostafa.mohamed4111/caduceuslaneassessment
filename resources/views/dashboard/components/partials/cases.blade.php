<div class="mdl-card mdl-shadow--2dp employer-form" action="#">
    <div class="mdl-card__title">
        <h2>Open New Case</h2>
    </div>

    <div class="mdl-card__supporting-text">
        <form action="{{ route('patients-cases.store') }}" method="post" class="form">
            @csrf
            <div class="form__article employer-form__general_skills">
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select full-size">
                    <select class="mdl-textfield__input" name="pain_type_id" required style="color: #00bcd4;background: #444;">
                        <option value="">Select Pain Type</option>
                        @foreach(\App\Model\PainType::query()->get() as $pain)
                            <option value="{{ $pain->id }}" {{ old('paint_type_id', auth()->user()->paint_type_id == $pain->id ? "selected" : "") }}>
                                {{ $pain->name }}</option>
                        @endforeach
                    </select>
                    @error('pain_type_id')
                    <div class="error">{{ $message }}</div>
                    @enderror
                </div>

                <h3>Case Description</h3>

                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label is-upgraded" data-upgraded=",MaterialTextfield">
                    <textarea class="mdl-textfield__input" type="text" rows="3" name="case_description"> </textarea>
                </div>
                @error('case_description')
                <div class="error">{{ $message }}</div>
                @enderror
            </div>

            <div class="form__action">
                <button type="submit" id="submit_button" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" data-upgraded=",MaterialButton">
                    Submit
                </button>
            </div>
        </form>
    </div>
</div>
<div class="mdl-grid ui-tables">
    <div class="mdl-cell mdl-cell--12-col-desktop mdl-cell--12-col-tablet mdl-cell--12-col-phone">
        <div class="mdl-card mdl-shadow--2dp">
            <div class="mdl-card__title">
                <h1 class="mdl-card__title-text">My Cases</h1>
            </div>
            <div class="mdl-card__supporting-text no-padding">
                <table class="mdl-data-table mdl-js-data-table bordered-table" data-upgraded=",MaterialDataTable">
                    <thead>
                    <tr>
                        <th class="mdl-data-table__cell--non-numeric">Reference #no</th>
                        <th class="mdl-data-table__cell--non-numeric">Pain</th>
                        <th class="mdl-data-table__cell--non-numeric">Description</th>
                        <th class="mdl-data-table__cell--non-numeric">Doctor</th>
                        <th class="mdl-data-table__cell--non-numeric">Appointment</th>
                        <th class="mdl-data-table__cell--non-numeric">Status</th>
                        <th class="mdl-data-table__cell--non-numeric">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($cases as $case)
                        <tr>
                            <td class="mdl-data-table__cell--non-numeric">{{ $case->reference }}</td>
                            <td class="mdl-data-table__cell--non-numeric">{{ $case->pain->name }}</td>
                            <td class="mdl-data-table__cell--non-numeric"><span class="limit-str">{{ $case->case_description }}</span></td>
                            <td class="mdl-data-table__cell--non-numeric">{{ optional($case->doctor)->name }}</td>
                            <td class="mdl-data-table__cell--non-numeric">{{ $case->appointment }}</td>
                            <td class="mdl-data-table__cell--non-numeric">
                                <ul>
                                    @if($case->doctor_confirmation == 1)
                                        <li>Doctor Confirm Appointment</li>
                                    @endif
                                </ul>
                            </td>
                            <td class="mdl-data-table__cell--non-numeric">
                                @if($case->appointment && $case->doctor_confirmation !== 2)
                                    @if($case->patient_confirmation == 0)
                                        <a href="{{ route('patients-cases.patientConfirmation' , ['patient_confirmation'=>1,'id'=>$case->id]) }}" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect button--colored-purple">Confirm</a>
                                        <a href="{{ route('patients-cases.patientConfirmation' , ['patient_confirmation'=>2,'id'=>$case->id]) }}" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect button--colored-red">Reschedule</a>
                                    @elseif($case->doctor_confirmation == 2)
                                        Doctor Need Reschedule Appointment
                                    @elseif($case->patient_confirmation == 1 && $case->doctor_confirmation == 1)
                                        Confirmed
                                        @else
                                        Witting Doctor Confirmation
                                    @endif
                                @elseif($case->doctor_confirmation == 2)
                                    Doctor Need Reschedule Appointment ( Witting Admin Action )
                                @else
                                    Witting Admin Action
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
