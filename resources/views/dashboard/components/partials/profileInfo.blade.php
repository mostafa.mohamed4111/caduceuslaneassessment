<div class="mdl-cell mdl-cell--6-col-desktop mdl-cell--6-col-tablet mdl-cell--6-col-phone">
    <div class="mdl-card mdl-shadow--2dp">
        <div class="mdl-card__title">
            <h5 class="mdl-card__title-text text-color--white">Complete PROFILE INFO</h5>
        </div>
        <div class="mdl-card__supporting-text">
            <form class="form form--basic" action="{{ route('user.update' , auth()->id()) }}" method="post">
                @csrf
                @method('PUT')
                <div class="mdl-grid">
                    <div class="mdl-cell mdl-cell--8-col-desktop mdl-cell--8-col-tablet mdl-cell--4-col-phone form__article">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-size">
                            <input class="mdl-textfield__input" type="text" value="{{ old('first_name', auth()->user()->first_name) }}" name="first_name" required autocomplete="off">
                            <label class="mdl-textfield__label" for="profile-floating-first-name">First Name</label>
                            @error('first_name')
                            <div class="error">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-size">
                            <input class="mdl-textfield__input" type="text" value="{{ old('last_name', auth()->user()->last_name) }}" name="last_name" required autocomplete="off">
                            <label class="mdl-textfield__label" for="profile-floating-last-name">Last Name</label>
                            @error('last_name')
                            <div class="error">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-size">
                            <input class="mdl-textfield__input" type="email" value="{{ old('email', auth()->user()->email) }}"  name="email" required autocomplete="off">
                            <label class="mdl-textfield__label" for="floating-e-mail">Email</label>
                            @error('email')
                            <div class="error">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-size">
                            <input class="mdl-textfield__input" type="text" value="{{ old('mobile',auth()->user()->mobile) }}" name="mobile" required autocomplete="off">
                            <label class="mdl-textfield__label" for="floating-e-mail">Mobile</label>
                            @error('mobile')
                            <div class="error">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-size">
                            <input class="mdl-textfield__input" id="datetime" type="text" value="{{ old('birthday',auth()->user()->birthday) }}" name="birthday" required autocomplete="off">
                            <label class="mdl-textfield__label" for="floating-e-mail">Birthday</label>
                            @error('birthday')
                            <div class="error">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-size">
                            <input class="mdl-textfield__input" type="text" value="{{ old('occupation',auth()->user()->occupation) }}" name="occupation" required autocomplete="off">
                            <label class="mdl-textfield__label" for="floating-e-mail">Occupation</label>
                            @error('occupation')
                            <div class="error">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select full-size">
                            <select class="mdl-textfield__input" name="gender" required style="color: #00bcd4;background: #444;">
                                <option value="">Select gender</option>
                                <option value="1" {{ old('gender', auth()->user()->gender == 1 ? "selected" : "") }}>Male</option>
                                <option value="2" {{ old('gender', auth()->user()->gender == 2 ? "selected" : "") }}>Female</option>
                            </select>
                            @error('gander')
                            <div class="error">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select full-size">
                            <select class="mdl-textfield__input" name="country_id" required style="color: #00bcd4;background: #444;">
                                <option value="">Select Country</option>
                                @foreach(\App\Model\Country::query()->get() as $country)
                                    <option value="{{ $country->id }}" {{ old('country_id', auth()->user()->paint_type_id == $country->id ? "selected" : "") }}>
                                        {{ $country->name }}</option>
                                @endforeach
                            </select>
                            @error('gander')
                            <div class="error">{{ $message }}</div>
                            @enderror
                        </div>

                        <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect button--colored-purple" data-upgraded=",MaterialButton,MaterialRipple">
                            Update
                            <span class="mdl-button__ripple-container"><span class="mdl-ripple is-animating" style="width: 262.161px; height: 262.161px; transform: translate(-50%, -50%) translate(62px, 4px);"></span></span></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@push('scripts')
    <script>
        $(function() {
            $('#datetime').datetimepicker({
                theme: 'dark'
            });
        })
    </script>
@endpush
