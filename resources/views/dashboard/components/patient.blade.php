@if(auth()->user()->needCompeteInfo())
    @include('dashboard.components.partials.profileInfo')
@else
    @include('dashboard.components.partials.cases')
@endif
