@extends('layouts.main')
@section('content')
    <main class="mdl-layout__content  mdl-color--grey-100">

        @include(auth()->user()->type)

    </main>
@endsection
