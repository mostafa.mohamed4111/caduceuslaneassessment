<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="icon" type="image/png" href="{{ asset('assets/') }}images/DB_16х16.png">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dashboard Lite</title>

    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">


    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Material Design Lite">


    <!-- Tile icon for Win8 (144x144 + tile color) -->
    <meta name="msapplication-TileImage" content="{{ asset('assets/') }}images/touch/ms-touch-icon-144x144-precomposed.png">
    <meta name="msapplication-TileColor" content="#3372DF">

    <!-- SEO: If your mobile URL is different from the desktop URL, add a canonical link to the desktop page https://developers.google.com/webmasters/smartphone-sites/feature-phones -->
    <!--
    <link rel="canonical" href="http://www.example.com/">
    -->

    <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,300,100,700,900' rel='stylesheet'
          type='text/css'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ asset('assets/css/lib/getmdl-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/lib/nv.d3.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/application.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/js/date/jquery.datetimepicker.min.css') }}">
    <!-- endinject -->

    <style>
        .limit-str {
            overflow: hidden;
            text-overflow: ellipsis;
            display: -webkit-box;
            -webkit-line-clamp: 2; /* number of lines to show */
            -webkit-box-orient: vertical;
            max-width: 100px;
        }
    </style>
</head>
<body>
@if(!isset($hideHeader))
    <div class="mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header is-small-screen">
        <header class="mdl-layout__header">
            <div class="mdl-layout__header-row">
                <div class="mdl-layout-spacer"></div>
                <!-- Search-->

                <div class="material-icons mdl-badge mdl-badge--overlap mdl-button--icon notification" id="notification"
                     data-badge="{{ auth()->user()->unreadNotifications->count() }}">
                    notifications_none
                </div>
                <!-- Notifications dropdown-->
                <ul class="mdl-menu mdl-list mdl-js-menu mdl-js-ripple-effect mdl-menu--bottom-right mdl-shadow--2dp notifications-dropdown"
                    for="notification">
                    <li class="mdl-list__item">
                        You have {{ auth()->user()->unreadNotifications->count() }} new notifications!
                    </li>
                    @foreach( auth()->user()->unreadNotifications as $notification)

                        <li class="mdl-menu__item mdl-list__item list__item--border-top">
                        <span class="mdl-list__item-primary-content">

                          <span>{{ $notification->data['message'] }}</span>
                        </span>
                            <span class="mdl-list__item-secondary-content">
                          <span class="label">{{ $notification->created_at->diffForHumans() }}</span>
                        </span>
                        </li>
                    @endforeach
                    <li class="mdl-list__item list__item--border-top">
                        <button href="#" class="mdl-button mdl-js-button mdl-js-ripple-effect">ALL NOTIFICATIONS</button>
                    </li>
                </ul>

                <div class="avatar-dropdown" id="icon">
                    <span>{{ auth()->user()->username }}</span>
                    <img src="{{ asset('assets/images/tudyk.png') }}">
                </div>
                <!-- Account dropdawn-->
                <ul class="mdl-menu mdl-list mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect mdl-shadow--2dp account-dropdown"
                    for="icon">
                    <li class="mdl-list__item mdl-list__item--two-line">
                    <span class="mdl-list__item-primary-content">
                        <span class="material-icons mdl-list__item-avatar"></span>
                        <span>{{ auth()->user()->username }}</span>
                        <span class="mdl-list__item-sub-title">{{ auth()->user()->email }}</span>
                    </span>
                    </li>
                    <li class="list__item--border-top"></li>
                    <a href="{{ route('logout') }}">
                        <li class="mdl-menu__item mdl-list__item">
                        <span class="mdl-list__item-primary-content">
                            <i class="material-icons mdl-list__item-icon text-color--secondary">exit_to_app</i>
                            Log out
                        </span>
                        </li>
                    </a>
                </ul>
            </div>
        </header>

        <div class="mdl-layout__drawer">
            <header>Caduceus Lane</header>
            <div class="scroll__wrapper" id="scroll__wrapper">
                <div class="scroller" id="scroller">
                    <div class="scroll__container" id="scroll__container">
                        <nav class="mdl-navigation">
                            <a class="mdl-navigation__link mdl-navigation__link--current" href="{{ route('home') }}">
                                <i class="material-icons" role="presentation">dashboard</i>
                                Dashboard
                            </a>

                            <div class="mdl-layout-spacer"></div>
                            <hr>
                            <a class="mdl-navigation__link" href="{{ route('logout') }}">
                                <i class="material-icons" role="presentation">link</i>
                                Logout
                            </a>
                        </nav>
                    </div>
                </div>
                <div class='scroller__bar' id="scroller__bar"></div>
            </div>
        </div>

        @yield('content')

    </div>
@else
    @yield('content')
@endif
<!-- inject:js -->
<script src="{{ asset('assets/js/jquery.js') }}"></script>
<script src="{{ asset('assets/js/d3.min.js') }}"></script>
<script src="{{ asset('assets/js/getmdl-select.min.js') }}"></script>
<script src="{{ asset('assets/js/material.min.js') }}"></script>
<script src="{{ asset('assets/js/nv.d3.min.js') }}"></script>
<script src="{{ asset('assets/js/layout/layout.min.js') }}"></script>
<script src="{{ asset('assets/js/scroll/scroll.min.js') }}"></script>
<script src="{{ asset('assets/js/widgets/charts/discreteBarChart.min.js') }}"></script>
<script src="{{ asset('assets/js/widgets/charts/linePlusBarChart.min.js') }}"></script>
<script src="{{ asset('assets/js/widgets/charts/stackedBarChart.min.js') }}"></script>
<script src="{{ asset('assets/js/widgets/employer-form/employer-form.min.js') }}"></script>
<script src="{{ asset('assets/js/widgets/line-chart/line-charts-nvd3.min.js') }}"></script>
<script src="{{ asset('assets/js/widgets/map/maps.min.js') }}"></script>
<script src="{{ asset('assets/js/widgets/pie-chart/pie-charts-nvd3.min.js') }}"></script>
<script src="{{ asset('assets/js/widgets/table/table.min.js') }}"></script>
<script src="{{ asset('assets/js/widgets/todo/todo.min.js') }}"></script>
<script src="{{ asset('assets/js/swal.js') }}"></script>
<script src="{{ asset('assets/js/date/jquery.datetimepicker.full.min.js') }}"></script>
<!-- endinject -->
@stack('scripts')
<script>
    $(function () {
        let success =   parseInt("{{ session()->has('success') }}");
        let error   =   parseInt("{{ session()->has('error') }}");
        if(success) {
            setTimeout(() => {
                Swal.fire({
                    type: 'success',
                    title: "{{ session()->get('success') }}",
                    animation: false,
                    customClass: 'animated swing'
                })
            } , 300)
        }

        if(error) {
            setTimeout(() => {
                Swal.fire({
                    type: 'error',
                    title: "{{ session()->get('error') }}",
                    animation: false,
                    customClass: 'animated swing'
                })
            } , 300)

        }

    })
</script>
</body>
</html>
