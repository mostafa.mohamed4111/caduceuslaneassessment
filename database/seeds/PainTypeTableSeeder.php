<?php

use Illuminate\Database\Seeder;

class PainTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Model\PainType::query()->truncate();
        \App\Model\PainType::query()->insert([
            ['name' =>  'Acute pain'],
            ['name' =>  'Chronic pain'],
            ['name' =>  'Breakthrough pain'],
            ['name' =>  'Bone pain'],
            ['name' =>  'Nerve pain'],
        ]);
    }
}
