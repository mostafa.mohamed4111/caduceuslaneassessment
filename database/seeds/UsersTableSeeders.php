<?php

use Illuminate\Database\Seeder;

class UsersTableSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::query()->truncate();

        \App\User::query()->create([
            'username'      =>  'admin',
            'first_name'    =>  'super',
            'last_name'     =>  'admin',
            'password'      =>  bcrypt(123456),
            'country_id'    =>  1,
            'user_type'     =>  \App\User::Admin
        ]);

        ###### create fake doctors
        $faker = \Faker\Factory::create();
        for($i=0;$i<20;$i++) {
            \App\User::query()->create([
                'username'      =>  $faker->userName,
                'first_name'    =>  $faker->firstName,
                'last_name'     =>  $faker->lastName,
                'mobile'        =>  $faker->phoneNumber,
                'gender'        =>  $faker->numberBetween(1,2),
                'birthday'      =>  $faker->dateTimeBetween('-50 years','-20 years'),
                'password'      =>  bcrypt(123456),
                'country_id'    =>  1,
                'user_type'     =>  \App\User::Doctor,
                'doctor_specialist_id'  =>  $faker->numberBetween(1,4)
            ]);
        }
    }
}
