<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Model\Country::query()->truncate();
        \App\Model\Country::query()->insert([
            ['name'  =>  'Egypt'],
            ['name'  =>  'USA'],
            ['name'  =>  'UAE'],
        ]);
    }
}
