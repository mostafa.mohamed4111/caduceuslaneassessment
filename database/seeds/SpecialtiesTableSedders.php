<?php

use Illuminate\Database\Seeder;

class SpecialtiesTableSedders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Model\Specialiest::query()->truncate();

        \App\Model\Specialiest::query()->insert([
            ['name'=>'Podiatric surgery'],
            ['name'=>'Ophthalmology'],
            ['name'=>'Neurology'],
            ['name'=>'Immunology'],
        ]);
    }
}
