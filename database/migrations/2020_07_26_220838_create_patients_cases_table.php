<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientsCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients_cases', function (Blueprint $table) {
            $table->id();
            $table->string('reference');
            $table->unsignedBigInteger('patient_id');
            $table->unsignedBigInteger('doctor_id')->nullable();
            $table->unsignedBigInteger('pain_type_id');
            $table->text('case_description');
            $table->dateTime('appointment')->nullable();
            $table->integer('status')->default(0);
            $table->integer('patient_confirmation')->default(0);
            $table->integer('doctor_confirmation')->default(0);
            $table->timestamps();

            $table->foreign('patient_id')->references('id')->on('users');
            $table->foreign('doctor_id')->references('id')->on('users');
            $table->foreign('pain_type_id')->references('id')->on('pain_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients_cases');
    }
}
